<!doctype html>
<html lang="en">
  <head>
    <title>Solar-Calculation!</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />	
    <link rel="stylesheet" href="css/style.css" />	
  </head>
  <body>
<div class="container">
	<div class="row">
	  <div class="col-md-6 offset-md-3 col-sm-6">
		<div class="header-text">			
			<h2>Solar-Calculation!</h2>
		</div>
		<div id="app">		
			<div class="app"> 
				<div class="row">
					<div class="col-md-6 col-sm-6">					
					<div class='margin-top'></div>
						<div class="settings">	  
						  <div class="settings__item">
							<label for="">Daily water consumption @ 40°C</label>
							<input type="text" name="f1" id="f1" placeholder="litres">							
						  </div>	   
						</div>
						<div class="settings">	  
						  <div class="settings__item">
							<label for="">Solar gain (50 - 60 %):</label>
							<input type="text" name="f2" id="f2" onClick="clear_result()" onkeyup="check_lengh(this.value)" maxlength="2" onkeydown="Javascript: if (event.keyCode == 13) find_result(); " placeholder="%">							
						  </div>	   
						</div>
					</div>				
					<div class="col-md-6 col-sm-6">					
						<div class="app__content">
							<p>Required storage capacity:</p>
							<div class="current">
								<p id="res">0</p>
								<a class='result'>litres</a>
							</div> 								
						</div>
					</div>
					<input type="hidden" value="Add" onclick="find_result();" />
				</div>
			</div>
		</div>
		<!--app two-->
		<div id="app">
			<div class="app"> 
				<div class="row">
					<div class="col-md-6 col-sm-6">					
						<div class="settings">	  
						  <div class="settings__item">
							<label for="">Recovery time for water storage 20 to 60°C</label>
							<input type="text" placeholder="hours" name="f3" id="f3">							
						  </div>	   
						</div>
						<div class="settings">	  
						  <div class="settings__item">
							<label for="">CWS temperature of domestic water:</label>
							<input type="text" placeholder="°C" name="f5" id="f5">							
						  </div>	   
						</div>
						<div class="settings">	  
						  <div class="settings__item">
							<label for="">HWS temperature of domestic water:</label>
							<input type="text" placeholder="°C" name="f4" id="f4" onClick="clear_result()" onkeyup="check_lengh2(this.value)" maxlength="2" onkeydown="Javascript: if (event.keyCode == 13) find_result2(); ">							
						  </div>	   
						</div>
					</div>				
					<div class="col-md-6 col-sm-6">					
						<div class="app__content">	
							<p>Required solar capacity:</p>						
							<div class="current">
							<p id="res2">0</p>
							<a class='result'>kW</a></div> 								
						</div>
					</div>
					<input type="hidden" value="Add" onclick="find_result2();" />
				</div>
			</div>
		</div> 		
	  </div>
	</div>	
</div>
<!-- Optional JavaScript -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/script.js"></script>
  </body>
</html>